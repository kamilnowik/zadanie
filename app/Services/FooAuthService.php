<?php

namespace App\Services;

use App\Services\Interfaces\AuthServiceInterface;
use External\Foo\Auth\AuthWS;
use External\Foo\Exceptions\AuthenticationFailedException;

class FooAuthService implements AuthServiceInterface
{
    /**
     * @var AuthWS
     */
    public $authWS;

    /**
     * @param AuthWS $authWS
     */
    public function __construct(AuthWS $authWS)
    {
        $this->authWS = $authWS;
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function checkAuth(string $login, string $password): bool
    {
        try {
            $this->authWS->authenticate($login, $password);
            return true;

        } catch (AuthenticationFailedException $e) {

            return false;
        }
    }
}
