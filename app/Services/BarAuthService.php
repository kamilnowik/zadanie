<?php

namespace App\Services;

use App\Services\Interfaces\AuthServiceInterface;
use External\Bar\Auth\LoginService;

class BarAuthService implements AuthServiceInterface
{

    protected $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function checkAuth(string $login, string $password): bool
    {
        return $this->loginService->login($login, $password);
    }
}
