<?php

namespace App\Services;

use App\Services\Interfaces\MovieServiceInterface;
use External\Foo\Exceptions\ServiceUnavailableException;
use External\Foo\Movies\MovieService;

class FooMovieService implements MovieServiceInterface
{
    /**
     * @var MovieService
     */
    protected $movieService;

    /**
     * @param MovieService $movieService
     */
    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    /**
     * @return array
     * @throws \App\Exceptions\ServiceUnavailableException
     */
    public function getTitle(): array
    {
        try{
            return $this->movieService->getTitles();
        }
        catch (ServiceUnavailableException $exception){
            throw new \App\Exceptions\ServiceUnavailableException();
        }
    }
}
