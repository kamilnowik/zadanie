<?php

namespace App\Services;

use App\Services\Interfaces\AuthServiceInterface;
use External\Baz\Auth\Authenticator;
use External\Baz\Auth\Responses\Success;

class BazAuthService implements AuthServiceInterface
{
    protected $authenticator;

    /**
     * @param Authenticator $authenticator
     */
    public function __construct(Authenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function checkAuth(string $login, string $password): bool
    {
        return $this->authenticator->auth($login, $password) instanceof Success;
    }
}
