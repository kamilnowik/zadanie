<?php

namespace App\Services;

use App\Services\Interfaces\MovieServiceInterface;
use External\Bar\Exceptions\ServiceUnavailableException;
use External\Bar\Movies\MovieService;

class BarMovieService implements MovieServiceInterface
{
    protected $movieService;

    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    public function getTitle(): array
    {
        try {
            return $this->movieService->getTitles();
        } catch (ServiceUnavailableException $exception) {
            throw new \App\Exceptions\ServiceUnavailableException();
        }
    }
}
