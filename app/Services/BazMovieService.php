<?php

namespace App\Services;

use App\Services\Interfaces\MovieServiceInterface;
use External\Baz\Exceptions\ServiceUnavailableException;
use External\Baz\Movies\MovieService;

class BazMovieService implements MovieServiceInterface
{

    protected $movieService;

    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    public function getTitle(): array
    {
        try {
            return $this->movieService->getTitles();
        } catch (ServiceUnavailableException $exception) {
            throw new \App\Exceptions\ServiceUnavailableException();
        }
    }
}
