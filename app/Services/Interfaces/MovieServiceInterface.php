<?php

namespace App\Services\Interfaces;

use App\Exceptions\ServiceUnavailableException;

interface MovieServiceInterface
{
    /**
     * @return array
     * @throws ServiceUnavailableException
     */
    public function getTitle(): array;
}
