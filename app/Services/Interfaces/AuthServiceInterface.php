<?php

namespace App\Services\Interfaces;

interface AuthServiceInterface
{
    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function checkAuth(string $login, string $password): bool;
}
