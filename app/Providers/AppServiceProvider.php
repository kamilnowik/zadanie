<?php

namespace App\Providers;

use App\Http\Controllers\MovieController;
use App\Services\BarAuthService;
use App\Services\BazAuthService;
use App\Services\FooAuthService;
use App\Services\Interfaces\AuthServiceInterface;
use App\Providers\MoviesAggregator;
use App\Services\Interfaces\MovieServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $request = app(Request::class);

        if (Str::startsWith($request->json('login'), 'FOO_')) {
            $this->app->bind(AuthServiceInterface::class, FooAuthService::class);
        } else if (Str::startsWith($request->json('login'), 'BAR_')) {
            $this->app->bind(AuthServiceInterface::class, BarAuthService::class);
        } else if (Str::startsWith($request->json('login'), 'BAZ_')) {
            $this->app->bind(AuthServiceInterface::class, BazAuthService::class);
        }


        $this->app->bind(\External\Bar\Movies\MovieService::class, function () {
            return new \External\Bar\Movies\MovieService();
        });

        $this->app->bind(\External\Baz\Movies\MovieService::class, function () {
            return new \External\Baz\Movies\MovieService();
        });

        $this->app->bind(\External\Foo\Movies\MovieService::class, function () {
            return new \External\Foo\Movies\MovieService();
        });

        $this->app->tag([
            \External\Bar\Movies\MovieService::class,
            \External\Baz\Movies\MovieService::class,
            \External\Foo\Movies\MovieService::class
        ], 'movies');

        $this->app->when(MovieController::class)->needs('$movieServices')->giveTagged('movies');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
