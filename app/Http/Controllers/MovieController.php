<?php

namespace App\Http\Controllers;

use App\Exceptions\ServiceUnavailableException;
use App\Providers\MoviesAggregator;
use App\Services\Interfaces\MovieServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MovieController extends Controller
{
    protected $movieServices;

    public function __construct($movieServices)
    {
        $this->movieServices = $movieServices;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getTitles(Request $request): JsonResponse
    {
        $titles = [];
        try {
            foreach ($this->movieServices as $movieService) {
                $titles[] = $movieService->getTitles();
            }

            return response()->json([Arr::flatten($titles)]);
        } catch (ServiceUnavailableException $e) {

            return response()->json(["status" => "failure"]);
        }
    }
}
