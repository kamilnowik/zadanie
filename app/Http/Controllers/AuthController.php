<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\AuthServiceInterface;
use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthServiceInterface $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        if ($this->authService->checkAuth($request->json('login'), $request->json('password'))) {

            $token = (new Builder())
                ->withClaim('login', $request->json('login'))
                ->withClaim('system', substr($request->json('login'), 0, 3)) //todo: wyniesc
                ->getToken();

            return response()->json([
                'status' => 'success',
                'token' => $token
            ]);
        }

        return response()->json([
            'status' => 'failure',
        ]);
    }
}
